package iam.thevoid.datastore.core

import kotlin.reflect.KClass

interface Serializer {
    fun <T : Any> toJson(obj: T, cls: KClass<T>): String
    fun <T : Any> fromJson(string: String, cls: KClass<T>): T
}
