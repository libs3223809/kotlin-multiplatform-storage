package iam.thevoid.datastore.core

import kotlin.reflect.KClass

interface KeyValueStorage {
    suspend fun <T : Any> put(key: String, value: T)
    suspend fun <T : Any> get(key: String, cls: KClass<T>): T?
    suspend fun <T : Any> get(key: String, default: T): T
    suspend fun delete(key: String)
}

suspend inline fun <reified T : Any> KeyValueStorage.get(key: String): T? = get(key, T::class)
