pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

includeBuild("./kmp-concurrenthashmap")

rootProject.name = "kotlin-multiplatform-storage"
include(":core")
include(":local")
include(":inmemory")
include(":observable")