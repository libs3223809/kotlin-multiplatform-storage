object Dependencies {

    object Plugin {
        const val multiplatform = "multiplatform"

        object Local {
            const val android = "android-library-convention"
            const val kotlin = "kotlin-convention"
            const val detekt = "detekt-convention"
        }
    }
}