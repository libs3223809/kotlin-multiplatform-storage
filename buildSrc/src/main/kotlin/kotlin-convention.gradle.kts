import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

tasks.withType<KotlinCompile> {
    compilerOptions {
        jvmTarget = Versions.jvmTarget
//            allWarningsAsErrors = true
    }
}
