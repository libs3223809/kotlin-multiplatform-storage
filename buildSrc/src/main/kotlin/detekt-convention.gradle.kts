import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.DetektCreateBaselineTask
import org.gradle.accessors.dm.LibrariesForLibs

val libs = the<LibrariesForLibs>()

plugins { id("io.gitlab.arturbosch.detekt") }

dependencies {
    detektPlugins(libs.detekt.plugin.formatting)
    detektPlugins(libs.detekt.plugin.api)
    detektPlugins(libs.detekt.plugin.cli)
}

detekt {
    toolVersion = libs.versions.detekt.get()

    config = files(File(rootProject.rootDir, "config/detekt/detekt.yml").path)
    baseline = File(rootProject.rootDir, "config/detekt/baseline.xml")
    source = files(
        "src/androidTest/java",
        "src/androidTest/kotlin",
        "src/debug/java",
        "src/debug/kotlin",
        "src/release/java",
        "src/release/kotlin",
        "src/main/kotlin",
        "src/main/java",
        "src/commonMain/kotlin",
        "src/androidMain/kotlin",
        "src/iosMain/kotlin"
    )
    parallel = true
    autoCorrect = true
}

tasks.withType<Detekt>().configureEach {
    jvmTarget = Versions.javaVersion.majorVersion

    reports {
        xml.required.set(true)
        xml.outputLocation.set(file("build/reports/detekt.xml"))

        txt.required.set(true)
        txt.outputLocation.set(file("build/reports/detekt.txt"))
    }
}


tasks.withType<DetektCreateBaselineTask>().configureEach {
    jvmTarget = Versions.javaVersion.majorVersion
}