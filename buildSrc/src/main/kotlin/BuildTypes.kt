enum class BuildTypes {
    release,
    debug;

    companion object {
        val values = BuildTypes.values().toList()
        val names = values.map { it.name }
    }
}