import com.android.build.api.dsl.LibraryExtension

plugins {
    id("com.android.library")
    id("android-common-convention")
}

android {
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")

    buildTypes {
        BuildTypes.names.forEach(::maybeCreate)
    }

//    lint {
//        xmlReport = true
//        xmlOutput = rootDirPrefixedFile("build/report/lint-report.xml")
//    }

}

fun Project.android(configure: Action<LibraryExtension>): Unit =
    extensions.configure("android", configure)