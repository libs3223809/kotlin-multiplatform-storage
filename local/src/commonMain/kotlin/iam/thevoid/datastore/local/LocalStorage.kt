package iam.thevoid.datastore.local

import com.russhwolf.settings.Settings
import iam.thevoid.datastore.core.KeyValueStorage
import iam.thevoid.datastore.core.Serializer
import kotlin.reflect.KClass

@Suppress("OVERRIDE_BY_INLINE")
class LocalStorage(
    private val settings: Settings,
    private val serializer: Serializer
) : KeyValueStorage {
    override suspend fun < T : Any> put(key: String, value: T) {
        val serialized = when (val cls = value::class as KClass<T>) {
            Double::class,
            Float::class,
            Long::class,
            Int::class,
            Short::class,
            Byte::class,
            Boolean::class
            -> value.toString()
            String::class -> value as String
            else -> serializer.toJson(value, cls)
        }
        settings.putString(key, serialized)
    }

    @Suppress("UNCHECKED_CAST")
    override suspend fun <T : Any> get(key: String, cls: KClass<T>): T? {
        return settings.getStringOrNull(key)?.let {
            when (cls) {
                Double::class -> it.toDouble() as T
                Float::class -> it.toFloat() as T
                Long::class -> it.toLong() as T
                Int::class -> it.toInt() as T
                Boolean::class -> it.toBoolean() as T
                Short::class -> it.toShort() as T
                Byte::class -> it.toByte() as T
                String::class -> it as T
                else -> serializer.fromJson(it, cls)
            }
        }
    }

    override suspend fun < T : Any> get(key: String, default: T): T {
        return get(key, default::class) ?: default
    }

    override suspend fun delete(key: String) {
        settings.remove(key)
    }
}
