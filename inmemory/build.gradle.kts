plugins {
    kotlin(Dependencies.Plugin.multiplatform)
    id(Dependencies.Plugin.Local.android)
    id(Dependencies.Plugin.Local.detekt)
}

group = "iam.thevoid.datastore.multiplatform"
version = 1.0

android {
    namespace = "iam.thevoid.bkkpr.datastore.inmemory"
}

kotlin {
    androidTarget()
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(project(":core"))
                implementation(libs.submodule.kmp.concurrentHashMap)
            }
        }
    }
}
