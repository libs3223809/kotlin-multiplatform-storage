package iam.thevoid.datastore.inmemory

import iam.thevoid.datastore.core.KeyValueStorage
import java.util.concurrent.ConcurrentHashMap
import kotlin.collections.set
import kotlin.reflect.KClass

class InMemoryStorage : KeyValueStorage {

    private val cache = ConcurrentHashMap<String, Any>()

    override suspend fun <T : Any> put(key: String, value: T) {
        cache[key] = value
    }

    @Suppress("UNCHECKED_CAST")
    override suspend fun <T : Any> get(key: String, cls: KClass<T>): T? {
        return cache[key] as? T
    }

    @Suppress("UNCHECKED_CAST")
    override suspend fun <T : Any> get(key: String, default: T): T {
        return cache[key] as? T ?: default
    }

    override suspend fun delete(key: String) {
        cache.remove(key)
    }
}
