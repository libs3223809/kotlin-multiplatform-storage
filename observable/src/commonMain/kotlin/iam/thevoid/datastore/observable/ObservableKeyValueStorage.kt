package iam.thevoid.datastore.observable

import iam.thevoid.datastore.core.KeyValueStorage
import kotlinx.coroutines.flow.Flow
import kotlin.reflect.KClass

interface ObservableKeyValueStorage : KeyValueStorage {
    fun <T : Any> observe(key: String, cls: KClass<T>): Flow<T?>
    fun <T : Any> observe(key: String, default: T): Flow<T>
}

inline fun <reified T : Any> ObservableKeyValueStorage.observe(key: String): Flow<T?> = observe(key, T::class)
