@file:OptIn(DelicateCoroutinesApi::class)

package iam.thevoid.datastore.observable

import iam.thevoid.datastore.core.KeyValueStorage
import iam.thevoid.multipatform.concurrenthashmap.ConcurrentHashMap
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import kotlin.reflect.KClass

class ObservableKeyValueStorageImpl(private val storage: KeyValueStorage) :
    ObservableKeyValueStorage {

    private var hasValue = false

    @OptIn(ExperimentalCoroutinesApi::class)
    private val scope = CoroutineScope(newSingleThreadContext(toString()))
    private val cache: ConcurrentHashMap<String, Any> = ConcurrentHashMap()
    private val event: MutableSharedFlow<Any> = MutableSharedFlow(replay = 10)

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> observe(key: String, cls: KClass<T>): Flow<T?> =
        event.map { cache.get(key) as? T }
            .distinctUntilChanged()
            .onStart { emit(get(key, cls)?.also { cache.put(key,it) }) }

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> observe(key: String, default: T): Flow<T> =
        event.map { cache.get(key) as? T ?: default }
            .distinctUntilChanged()
            .onStart { if (get(key, default::class) == null) storage.put(key, default) }
            .onStart { emit(get(key, default).also { cache.put(key, it) }) }

    override suspend fun <T : Any> put(key: String, value: T) {
        storage.put(key, value)
        notify(key, value)
    }

    override suspend fun <T : Any> get(key: String, cls: KClass<T>): T? {
        return storage.get(key, cls)
    }

    override suspend fun <T : Any> get(key: String, default: T): T {
        return storage.get(key, default)
    }

    override suspend fun delete(key: String) {
        storage.delete(key)
        notifyRemove(key)
    }

    private fun <T : Any> notify(key: String, value: T) {
        scope.launch {
            cache.put(key, value)
            event.emit(Any())
            hasValue = true
        }
    }

    private fun notifyRemove(key: String) {
        scope.launch {
            cache.remove(key)
            event.emit(Any())
        }
    }
}
