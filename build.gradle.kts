buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath(libs.gradle.plugin.kotlin)
        classpath(libs.gradle.plugin.android)
    }
}

allprojects {
    configurations {
        all {
//            exclude(module = "")
        }
    }

    apply(plugin = Dependencies.Plugin.Local.kotlin)
}